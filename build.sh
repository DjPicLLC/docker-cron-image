#!/bin/bash

# What base image of Alpine to use for all builds
alpine_version=$1


if [[ -v alpine_version ]]; then
  # Build standard cron image
  docker build --build-arg alpine_version=$alpine_version --tag djpic/cron:$alpine_version-standard --tag $CI_REGISTRY_IMAGE/cron:$alpine_version-standard .
  docker tag djpic/cron:$alpine_version-standard djpic/cron:latest
  docker tag $CI_REGISTRY_IMAGE/cron:$alpine_version-standard $CI_REGISTRY_IMAGE/cron:latest


  # Build php cron image
  cd php
  docker build --build-arg alpine_version=$alpine_version --tag djpic/cron:$alpine_version-php --tag $CI_REGISTRY_IMAGE/cron:$alpine_version-standard .

  # Build development cron image
  cd ../development/
  docker build --build-arg alpine_version=$alpine_version --tag djpic/cron:$alpine_version-dev --tag $CI_REGISTRY_IMAGE/cron:$alpine_version-dev .
  docker tag djpic/cron:$alpine_version-dev djpic/cron:dev
  docker tag $CI_REGISTRY_IMAGE/cron:$alpine_version-dev $CI_REGISTRY_IMAGE/cron:dev

  # Push images to Dockerhub
  docker push djpic/cron:latest
  docker push djpic/cron:dev
  docker push djpic/cron:php
  docker push djpic/cron:$alpine_version-standard
  docker push djpic/cron:$alpine_version-php
  docker push djpic/cron:$alpine_version-dev

  docker push $CI_REGISTRY_IMAGE/cron:latest
  docker push $CI_REGISTRY_IMAGE/cron:dev
  docker push $CI_REGISTRY_IMAGE/cron:php
  docker push $CI_REGISTRY_IMAGE/cron:$alpine_version-standard
  docker push $CI_REGISTRY_IMAGE/cron:$alpine_version-php
  docker push $CI_REGISTRY_IMAGE/cron:$alpine_version-dev

else
  echo "Alpine Version Missing"
  exit 1
fi
